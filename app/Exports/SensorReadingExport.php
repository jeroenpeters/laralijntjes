<?php

namespace App\Exports;

use App\Sensor;
use App\Temperature;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SensorReadingExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function __construct(int $sensor_id = 0)
    {
        $this->sensor = $sensor_id;
    }

    public function query()
    {
        if(empty($this->sensor))
        {
            return Temperature::query();
        }

        $sensor = Sensor::where('id', $this->sensor)->first();
        return $sensor->readings();
    }

    public function headings(): array
    {
        return [
            'ID',
            'Sensor nummer',
            'Temperatuur',
            'Luchtvochtigheid',
            'Datum'
        ];
    }
}