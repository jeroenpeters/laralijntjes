<?php

namespace App\Http\Controllers;

use App\Temperature;
use App\Sensor;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('start', [
            'max_24h' => Temperature::where('date', '>=', Carbon::now()->subDay())->max('temperature'),
            'min_24h' => Temperature::where('date', '>=', Carbon::now()->subDay())->min('temperature'),
            'avg_24h' => number_format(Temperature::where('date', '>=', Carbon::now()->subDay()->toDateTimeString())->avg('temperature'), 2, '.', ','),
            'sensors_active' => Sensor::where('active', 1),
            'sensors_with_reading' => Temperature::where('date', '>=', Carbon::now()->subDay())->distinct('sensor_id')->count('sensor_id'),
            'time_since_24h' => Carbon::now()->subDay()->toDateTimeString(),
            'has_readings_24h' => Temperature::where('date', '>=', Carbon::now()->subDay())->count(),
            'chart_colors' => [
                ['rgba(60,141,188,0.9)', 'rgba(60,141,188,0.8)','#3b8bba', 'rgba(60,141,188,1)', '#fff', 'rgba(60,141,188,1)'],
                ['rgba(210, 214, 222, 1)', 'rgba(210, 214, 222, 1)', 'rgba(210, 214, 222, 1)', '#c1c7d1', '#fff', 'rgba(220,220,220,1)'],
                ['rgba(60,141,188,0.9)', 'rgba(60,141,188,0.8)','#3b8bba', 'rgba(60,141,188,1)', '#fff', 'rgba(60,141,188,1)'],
            ],
        ]);
    }

}


