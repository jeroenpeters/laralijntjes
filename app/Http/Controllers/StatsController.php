<?php

namespace App\Http\Controllers;

use App\Temperature;
use App\Sensor;
use App\Exports\SensorReadingExport;


class StatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $temperature = Temperature::orderBy('date', 'desc')->paginate(24);
        return view('stats/table', ['temperature' => $temperature]);
    }

    public function graph()
    {
        $temperature = Temperature::orderBy('date', 'desc')->take(3000)->get();
        return view('graph', compact('temperature'));
    }

    public function sensor_stats($sensor_id)
    {
        $sensor = Sensor::where('id', $sensor_id)->first();
        $readings = $sensor->readings()->orderBy('date', 'desc')->paginate(24);
        return view('stats/sensor', ['sensor' => $sensor, 'readings' => $readings]);
    }

    public function download_sensor_stats($sensor_id)
    {
        return (new SensorReadingExport($sensor_id))->download('temperature_readings.xlsx');
    }

    public function download_all_sensor_stats()
    {
        return (new SensorReadingExport(0))->download('temperature_readings.xlsx');
    }

}
