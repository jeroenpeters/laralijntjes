<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use App\Sensor;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $sensors = Sensor::where('active', 1)->get();

            if(count($sensors))
            {
                $event->menu->add('Directe sensormeting');
            }

            foreach ($sensors as $sensor) {
                $event->menu->add([
                    'text' => $sensor['name'],
                    'url' => route('sensor_stats', $sensor),
                    'icon' => 'thermometer-half ' . ($sensor->has_recent_readings()==0?'red':'green')
                ]);
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
