<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{

    protected $table = 'temperature_sensor';
    public $timestamps = false;

    /**
     * Get the readings for a sensor
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function readings()
    {
        return $this->hasMany('App\Temperature');
    }

    public function reading_count()
    {
        return $this->readings()->count();
    }

    public function has_recent_readings()
    {
        return $this->readings()->where('date', '>=', Carbon::now()->subDay())->count();
    }

    public function readings_last_week()
    {
        return $this->readings()->where('date', '>=', Carbon::now()->subDay(7))->get();
    }
}
