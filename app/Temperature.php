<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Temperature extends Model
{

    protected $table = 'temperature_log';
    public $timestamps = false;

    /**
     * Get the sensor data record associated with the user.
     */
    public function sensor()
    {
        return $this->hasOne('App\Sensor');
    }

    public function last_week()
    {
        return $this->where('date', '>=', Carbon::now()->subDay(7))->get();
    }

}
