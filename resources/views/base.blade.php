@extends('adminlte::page')


@section('css')
    <style>
        .sidebar-menu>li>a>.fa.red {
            color: red;
        }
        .sidebar-menu>li>a>.fa.green {
            color: green;
        }
    </style>
@stop