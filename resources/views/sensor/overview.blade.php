@extends('base')

@section('title', 'Sensor overzicht')

@section('content_header')
    <h1>Sensor overzicht</h1>
@stop

@section('content')
    <p>Overzicht van alle sensors bekend in het systeem</p>

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Sensor overzicht</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <thead>
                    <tr>
                        <th>Sensor Nummer</th>
                        <th>Naam</th>
                        <th>Beschrijving</th>
                        <th>Verstuurt metingen</th>
                        <th>Totaal metingen</th>
                        <th>Status</th>
                        <th>Acties</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($sensors as $sensor)
                        <tr>
                            <td><a href="#">{{ $sensor->id }}</a></td>
                            <td>{{ $sensor->name }}</td>
                            <td>{{ $sensor->description }}</td>
                            <td>
                                @if ($sensor->has_recent_readings() > 0)
                                    <span class="label label-success">Ja!</span>
                                @else
                                    <span class="label label-danger">Nee</span>
                                @endif
                            </td>
                            <td>{{ $sensor->reading_count() }}</td>
                            <td>
                                @if ($sensor->active == 1)
                                    <span class="label label-success">Actief</span>
                                @else
                                    <span class="label label-danger">Niet actief</span>
                                @endif
                            </td>
                            <td><a href="/stats/sensor/{{ $sensor->id }}"><i class="fa fa-table"></i> Bekijk metingen</a></td>
                            {{--<td>--}}
                                {{--<div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>--}}
                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="box-footer clearfix">
            <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Nieuwe sensor toevoegen</a>
        </div>
    </div>

@stop
