@extends('base')

@section('title', 'Temperatuur statistieken')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    <!-- Info boxes -->

    @if (empty($has_readings_24h))
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Geen metingen gezien in de laatste 24 uur!</h4>
            Er zijn in de laatste 24 uur geen metingen meer binnengekomen van de actieve sensors op het dashboard.
            Het beste is om &eacute;&eacute;n van de sensors even te resetten (stekker eruit en erin) om te kijken
            of dit het probleem oplost.
        </div>
    @endif


    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="ion ion-thermometer"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Gemiddelde temperatuur laatste 24h</span>
                    <span class="info-box-number">{{ $avg_24h }} <small>&#8451;</small></span>
                    <span class="info-box-text"><small>Vanaf {{ $time_since_24h }}</small></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-analytics"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Laagst gemeten</span>
                    <span class="info-box-number">
                        @if (! empty($has_readings_24h))
                            {{ $min_24h }} <small>&#8451;</small>
                        @else
                            <small>Geen metingen van de afgelopen 24 uur!</small>
                        @endif
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-analytics-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Hoogst gemeten</span>

                    <span class="info-box-number">
                        @if (! empty($has_readings_24h))
                            {{ $max_24h }} <small>&#8451;</small>
                        @else
                            <small>Geen metingen van de afgelopen 24 uur!</small>
                        @endif
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-location"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Sensoren</span>
                    <span class="info-box-number">{{ $sensors_active->count() }} actief / {{ $sensors_with_reading }} meten</span>
                    <span class="info-box-more"><a href="/sensor">Bekijk sensors</a></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <!-- /.col (LEFT) -->
        <div class="col-md-12">
            <!-- LINE CHART -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Line Chart</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="lineChart" style="height:250px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col (RIGHT) -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Area Chart</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="areaChart" style="height:250px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>
    <!-- /.row -->

@stop

@section('js')

    <script src="/js/chart.js"></script>

    <script>
        $(function () {
            //--------------
            //- AREA CHART -
            //--------------

            var areaChartCanvas = $('#areaChart').get(0).getContext('2d');
            var areaChart       = new Chart(areaChartCanvas);

            var areaChartData = {
                labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [

                    @foreach($sensors_active->get() as $sensor)
                    {
                        label               : '{{ $sensor['name'] }}',
                        fillColor           : '{{ $chart_colors[$loop->index][0] }}',
                        strokeColor         : '{{ $chart_colors[$loop->index][1] }}',
                        pointColor          : '{{ $chart_colors[$loop->index][2] }}',
                        pointStrokeColor    : '{{ $chart_colors[$loop->index][3] }}',
                        pointHighlightFill  : '{{ $chart_colors[$loop->index][4] }}',
                        pointHighlightStroke: '{{ $chart_colors[$loop->index][5] }}',
                        data                : [
                        @foreach($sensor->readings_last_week() as $reading)
                            {{ $reading['temperature'] }},
                        @endforeach
                        ]
                    },
                    @endforeach
                ]
            };

            var areaChartOptions = {
                //Boolean - If we should show the scale at all
                showScale               : true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines      : true,
                //String - Colour of the grid lines
                scaleGridLineColor      : 'rgba(0,0,0,.05)',
                //Number - Width of the grid lines
                scaleGridLineWidth      : 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines  : true,
                //Boolean - Whether the line is curved between points
                bezierCurve             : true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension      : 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot                : true,
                //Number - Radius of each point dot in pixels
                pointDotRadius          : 2,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth     : 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke           : true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth      : 1,
                //Boolean - Whether to fill the dataset with a color
                datasetFill             : true,
                //String - A legend template
                legendTemplate          : '<ul class="name-legend"><li><span style="background-color:blue;"></span>Henk</li><li><span style="background-color:red;"></span>Spullen</li><li><span style="background-color:red;"></span>Spullen</li></ul>',
                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio     : true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive              : true
            };

            //Create the line chart
            areaChart.Line(areaChartData, areaChartOptions);

            //-------------
            //- LINE CHART -
            //--------------
            var lineChartCanvas          = $('#lineChart').get(0).getContext('2d');
            var lineChart                = new Chart(lineChartCanvas);
            var lineChartOptions         = areaChartOptions;
            lineChartOptions.datasetFill = false;
            lineChart.Line(areaChartData, lineChartOptions);
          });
    </script>

@stop