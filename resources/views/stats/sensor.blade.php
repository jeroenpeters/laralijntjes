@extends('base')


@section('title', 'Statistieken tabel totaal')

@section('content_header')
    <h1>
        Metingen
        <small>Totaaloverzicht van metingen van sensor {{ $sensor->name }}</small>
    </h1>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="box-title">Metingen van {{ $sensor->name }}</h2>

                    <div class="pull-right">
                        <a class="btn btn-primary" href="/stats/sensor/{{ $sensor->id }}/export">
                            <i class="fa fa-save"></i> Download in Excel
                        </a>
                    </div>
                </div>

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Datum</th>
                            <th>Temperatuur</th>
                            <th>Luchtvochtigheid</th>
                            <th>Sensor ID</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $readings as $temperature_entry )
                            <tr>
                                <th scope="row">{{ $temperature_entry->date }}</th>
                                <td>{{ $temperature_entry->temperature }} &#8451;</td>
                                <td>{{ $temperature_entry->humidity }}%</td>
                                <td>{{ $temperature_entry->sensor_id }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer clearfix">
                {{ $readings->links() }}
            </div>
        </div>
    </div>

@endsection