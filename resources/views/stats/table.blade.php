@extends('base')


@section('title', 'Statistieken tabel totaal')

@section('content_header')
    <h1>Totaaloverzicht van alle statistieken in tabelvorm</h1>
@stop

@section('content')
    <h2>Metingen overzicht <sup>({{ $temperature->count() }} metingen)</sup></h2>


    @if ( !$temperature->count() )
        Er is geen meting geweest.
    @else


        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Laatste 200 metingen</h3>

                        <div class="box-tools">
                            <!--
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                            -->
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Datum</th>
                                <th>Temperatuur</th>
                                <th>Luchtvochtigheid</th>
                                <th>Sensor ID</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $temperature as $temperature_entry )
                                <tr>
                                    <th scope="row">{{ $temperature_entry->date }}</th>
                                    <td>{{ $temperature_entry->temperature }} &#8451;</td>
                                    <td>{{ $temperature_entry->humidity }}%</td>
                                    <td>{{ $temperature_entry->sensor_id }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    {{ $temperature->links() }}
                </div>
            </div>
    @endif
@endsection