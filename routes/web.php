<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'dashboard/', 301);
Route::get('dashboard/', 'DashboardController@index')->name('dashboard');

/* Sensor management */
Route::get('sensor/', 'SensorController@index')->name('sensors');

/* Stats */
Route::get('stats/', 'StatsController@index')->name('stats');
Route::get('stats/export', 'StatsController@download_all_sensor_stats')->name('download_all_stats');
Route::get('stats/sensor/{sensor_id}', 'StatsController@sensor_stats')->name('sensor_stats');
Route::get('stats/sensor/{sensor_id}/export', 'StatsController@download_sensor_stats')->name('download_sensor_stats');

